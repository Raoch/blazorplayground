﻿using _7Side.DataAccess.Models;
using System;
using System.Collections.Generic;

namespace _7Side.Models
{
    public class Game
    {
        [DBPrimaryKeyAttribute]
        public int GameID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TimeOfGame { get; set; }
        public int GameTypeID { get; set; }
        public bool IsPrivate { get; set; }
        public int MinNumberOfPlayers { get; set; }
        public int MaxNumberOfPlayers { get; set; }
        [DBIgnoreAttribute]
        public IEnumerable<Player> Players { get; set; }
        [DBIgnoreAttribute]
        public bool IsSelected { get; set; }
    }
}
