﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _7Side.Models
{
    public class PaginatedResultViewModel<T>
    {
        public int NumberOfRows { get; set; }
        public int PageNumber { get; set; }
        public int TotalPages { get; set; }
        public IEnumerable<T> Results{ get; set; }
    }
}
