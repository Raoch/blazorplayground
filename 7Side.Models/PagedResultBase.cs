﻿
using System.Collections.Generic;

namespace _7Side.Models
{
    public abstract class PagedResultBase
    {
        public PagedResultBase()
        {
            this.CurrentPage = 1;
            this.PageSize = 15;
            this.OrderBy = "";
            this.SearchFilters = new Dictionary<string, string>();
        }
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }
        public string OrderBy { get; set; }
        public bool OrderAsc { get; set; }
        public string SearchValue { get; set; }
        public string SearchField { get; set; }
        public Dictionary<string, string> SearchFilters { get; set; }
    }
}
