﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _7Side.Models
{
    public class TodoItem
    {
        public string Title { get; set; }
        public bool IsDone { get; set; }
    }
}
