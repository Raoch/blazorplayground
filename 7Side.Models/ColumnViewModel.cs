﻿using System.Collections.Generic;

namespace _7Side.Models
{
    public class ColumnViewModel
    {
        public ColumnViewModel()
        {
            DropDownFilters = new List<string>();
        }
        public string Name { get; set; }
        public List<string> DropDownFilters { get; set; }
    }
}
