﻿using System.Collections.Generic;

namespace _7Side.Models
{
    public class PagedResultViewModel<T> : PagedResultBase 
    {
        public IList<T> Results { get; set; }

        public PagedResultViewModel() : base()
        {
            Results = new List<T>();
        }
    }
}
