﻿using System;

namespace _7Side.Models
{
    public class Player
    {
        public int PlayerID { get; set; }
        public int UserID { get; set; }
        public int GameID { get; set; }
        public int TeamID { get; set; }
        public string Name { get; set; }
    }
}
