﻿
namespace _7Side.Models
{
    public class PaginatedRequestViewModel
    {
        public int NumberOfRows { get; set; }
        public int Page { get; set; }
        public string OrderBy { get; set; }
    }
}
