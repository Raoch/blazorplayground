CREATE TABLE `weatherforecast` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime DEFAULT NULL,
  `TemperatureC` int(11) DEFAULT NULL,
  `TemperatureF` int(11) DEFAULT NULL,
  `Summary` varchar(50) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
