CREATE TABLE `game` (
  `GameID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(45) NOT NULL,
  `Description` varchar(400) NOT NULL,
  `TimeOfGame` datetime DEFAULT NULL,
  `GameTypeID` varchar(45) NOT NULL,
  `IsPrivate` bit(1) NOT NULL,
  `MinNumberOfPlayers` int(11) NOT NULL,
  `MaxNumberOfPlayers` int(11) NOT NULL,
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`GameID`),
  UNIQUE KEY `GameID_UNIQUE` (`GameID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
