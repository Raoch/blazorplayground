CREATE TABLE ApplicationUser
(
    Id INT NOT NULL PRIMARY KEY,
    UserName NVARCHAR(256) NOT NULL,
    NormalizedUserName NVARCHAR(256) NOT NULL,
    Email NVARCHAR(256) NULL,
    NormalizedEmail NVARCHAR(256) NULL,
    EmailConfirmed BIT NOT NULL,
    PasswordHash NVARCHAR(200) NULL,
    PhoneNumber NVARCHAR(50) NULL,
    PhoneNumberConfirmed BIT NOT NULL,
    TwoFactorEnabled BIT NOT NULL
);
 
 
CREATE INDEX IX_ApplicationUser_NormalizedUserName ON ApplicationUser (NormalizedUserName);
 
 
CREATE INDEX IX_ApplicationUser_NormalizedEmail ON ApplicationUser (NormalizedEmail);