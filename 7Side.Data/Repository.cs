﻿using _7iSde.IData;
using _7Side.DataAccess.Models;
using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace _7Side.DataAccess
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly string tableName;
        private readonly string connectionString;
        private readonly string primaryKeyName;

        public Repository(string connectionString)
        {
            tableName = typeof(T).Name;
            this.connectionString = connectionString;

            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.GetCustomAttribute(typeof(DBPrimaryKeyAttribute)) != null)
                {
                    primaryKeyName = property.Name;
                    break;
                }
            }
        }

        public virtual T Get(string searchField, object value)
        {
            using (var connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    var sql = string.Format("SELECT * FROM {0} WHERE {1} = @searchValue", tableName, searchField);
                    object parameters = new { searchValue = value };
                    return connection.Query<T>(sql, param: parameters, transaction: transaction).FirstOrDefault();
                }
            }
        }

        public virtual IEnumerable<T> GetAll()
        {
            using (var connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    var sql = string.Format("SELECT * FROM {0}", tableName);
                    return connection.Query<T>(sql, transaction: transaction);
                }
            }
        }

        public virtual IEnumerable<T> ExecuteSql(string sql)
        {
            using (var connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    return connection.Query<T>(sql, transaction: transaction);
                }
            }
        }

        public virtual int Insert<T1>(T entity)
        {
            using (var connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    string valueNames = "";
                    string values = "";

                    PropertyInfo[] properties = typeof(T1).GetProperties();

                    foreach (PropertyInfo property in properties)
                    {
                        //if (property.GetCustomAttribute(typeof(DBIgnoreAttribute)) != null)
                        //    continue;

                        valueNames = string.Format("{0}{1},", valueNames, property.Name);
                        values = string.Format("{0}@{1},", values, property.Name);
                    }
                    var query = string.Format("INSERT INTO {0} ({1}) VALUES ({2}); SELECT LAST_INSERT_ID();", typeof(T1).Name, valueNames.Trim(','), values.Trim(','));
                    return connection.ExecuteScalar<int>(query, param: entity, transaction: transaction);
                }
            }
        }

        public int Count(string whereClause, object parameters)
        {
            using (var connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    return connection.ExecuteScalar<int>(string.Format("Select COUNT(*) from {0} WHERE {1}", tableName, whereClause), parameters, transaction);
                }
            }
        }
    }
}
