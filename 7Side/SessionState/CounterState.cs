﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _7Side.SessionState
{
    public class CounterState
    {
        public int CurrentCount { get; set; }
        public string LoggedInUser { get; set; }
    }
}
