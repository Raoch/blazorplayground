﻿

namespace _7Side.ViewModels
{
    public interface ITableItem
    {
        bool IsSelected { get; set; }
    }
}
