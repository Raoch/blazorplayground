using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using _7iSde.IDataAccess;
using _7Side.DataAccess;
using _7Side.Models;
using _7Side.IServices;
using _7Side.Services;
using _7Side.SessionState;

namespace _7Side
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString("connectionString");

            services.AddSingleton<IGameService, GameService>();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            // ** SESSION STATE
            services.AddScoped<CounterState>();

            services.AddTransient<IRepository<User>, Repository<User>>(x => new Repository<User>(connection));
            services.AddTransient<IRepository<Player>, Repository<Player>>(x => new Repository<Player>(connection));
            services.AddTransient<IRepository<Game>, Repository<Game>>(x => new Repository<Game>(connection));
            services.AddTransient<IRepository<WeatherForecast>, Repository<WeatherForecast>>(x => new Repository<WeatherForecast>(connection));

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IPlayerService, PlayerService>();
            services.AddTransient<IWeatherForecastService, WeatherForecastService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
                // ******
                // BLAZOR COOKIE Auth Code (begin)
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
                // BLAZOR COOKIE Auth Code (end)
                // ******
            });
        }
    }
}
