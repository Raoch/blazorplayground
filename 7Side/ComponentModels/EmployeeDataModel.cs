﻿namespace _7Side.ComponentModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using _7Side.IServices;
    using _7Side.Models;
    using Microsoft.AspNetCore.Components;
    using Microsoft.AspNetCore.Components.Web;

    public class EmployeeDataModel : ComponentBase
    {
        [Inject]
        protected IGameService gameService { get; set; }
        [Inject]
        protected IPlayerService playerService { get; set; }
        protected IEnumerable<Game> gameList;
        protected Game game;
        protected bool showCollapse;

        protected string modalTitle { get; set; }
        protected Boolean isDelete = false;
        protected Boolean isAdd = false;
        protected IEnumerable<Player> playerList;
        protected Player player;
        protected void CollapseTest()
        {
            showCollapse = !showCollapse;
        }
        protected string SearchString { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await GetGames();
            await GetPlayers();
        }

        protected async Task GetGames()
        {
            gameList = gameService.GetAll();
        }

        protected async Task GetPlayers()
        {
            playerList = playerService.GetAll();
        }

        protected async Task GetPlayersByGameID(int gameID)
        {
            playerList = playerService.GetPlayersByGameID(gameID);
        }

        protected async Task FilterEmp()
        {
            await GetGames();
            if (SearchString != "")
            {
                gameList = gameList.Where(x => x.Title.IndexOf(SearchString, StringComparison.OrdinalIgnoreCase) != -1)?.ToList();
            }
        }

        protected void AddGame()
        {
            game = new Game();
            this.modalTitle = "Add Employee";
            this.isAdd = true;
        }

        protected async Task EditEmployee(int gameID)
        {
            game = gameService.GetByID(gameID);
            this.modalTitle = "Edit Game";
            this.isAdd = true;
        }

        protected async Task SaveGame()
        {
            if (game.GameID != 0)
            {
                await Task.Run(() =>
                {
                    gameService.Edit(game);
                });
            }
            else
            {
                await Task.Run(() =>
                {
                    gameService.Insert(game);
                });
            }
            this.isAdd = false;
            await GetPlayers();
        }

        protected async Task DeleteConfirm(int empID)
        {
            //emp = await employeeService.Details(empID);
            this.isDelete = true;
        }

        protected async Task DeleteEmployee(int empID)
        {
            await Task.Run(() =>
            {
            //    employeeService.Delete(empID);
            });
            this.isDelete = false;
            await GetPlayers();
        }
        protected void closeModal()
        {
            this.isAdd = false;
            this.isDelete = false;
        }
    }
}
