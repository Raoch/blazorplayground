﻿using _7iSde.IDataAccess;
using _7Side.IServices;
using _7Side.Models;
using System.Collections.Generic;

namespace _7Side.Services
{
    public class PlayerService : IPlayerService
    {
        IRepository<Player> playerRepo;

        public PlayerService(IRepository<Player> playerRepo)
        {
            this.playerRepo = playerRepo;
        }

        public IEnumerable<Player> GetAll()
        {
            return playerRepo.GetAll();
        }
        public Player GetByID(int id)
        {
            return playerRepo.Get("PlayerID", id);
        }

        public IEnumerable<Player> GetPlayersByGameID(int gameID)
        {
            return playerRepo.GetAllWhere("GameID", gameID);
        }

        public int Insert(Player player)
        {
            return playerRepo.Insert<Player>(player);
        }
    }
}
