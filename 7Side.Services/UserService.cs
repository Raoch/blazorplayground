﻿using _7iSde.IDataAccess;
using _7Side.IServices;
using _7Side.Models;
using System.Collections.Generic;

namespace _7Side.Services
{
    public class UserService : IUserService
    {
        IRepository<User> userRepo;

        public UserService(IRepository<User> gameRepo)
        {
            this.userRepo = gameRepo;
        }

        public IEnumerable<User> GetAll()
        {
            return userRepo.GetAll();
        }
        public User GetByID(int id)
        {
            return userRepo.Get("UserID", id);
        }
        public int Insert(User user)
        {
            return userRepo.Insert<User>(user);
        }
    }
}
