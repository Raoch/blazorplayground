﻿using _7iSde.IDataAccess;
using _7Side.IServices;
using _7Side.Models;
using System.Collections.Generic;

namespace _7Side.Services
{
    public class GameService : IGameService
    {
        IRepository<Game> gameRepo;

        public GameService(IRepository<Game> gameRepo)
        {
            this.gameRepo = gameRepo;
        }

        public IEnumerable<Game> GetAll()
        {
            return gameRepo.GetAll();
        }
        public Game GetByID(int id)
        {
            return gameRepo.Get("GameID", id);
        }
        public int Insert(Game game)
        {
            return gameRepo.Insert<Game>(game);
        }
        public Game Edit(Game game)
        {
            return gameRepo.Update(game);
        }
    }
}
