using _7iSde.IDataAccess;
using _7Side.IServices;
using _7Side.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _7Side.Services
{
    public class WeatherForecastService : IWeatherForecastService
    {
        IRepository<WeatherForecast> weatherRepo;
        public WeatherForecastService(IRepository<WeatherForecast> weatherRepo)
        {
            this.weatherRepo = weatherRepo;
        }
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };
        public IEnumerable<WeatherForecast> GetForecastAsync(string currentUser)
        {
            // Get Weather Forecasts  
            IEnumerable<WeatherForecast> colWeatherForcasts = weatherRepo.GetAllWhere("Username", currentUser);
            return colWeatherForcasts;
        }
        public PagedResultViewModel<WeatherForecast> GetForecastsPaged(PagedResultBase request)
        {
            int totalRows = 0;
            request.SearchField = "Summary";
            IEnumerable<WeatherForecast> colWeatherForcasts = weatherRepo.GetAllPaginatedWhere(request.PageSize, request.CurrentPage, request.OrderBy, request.SearchField, request.SearchValue, request.OrderAsc);
            if (string.IsNullOrEmpty(request.SearchValue)){
                totalRows = weatherRepo.Count("Username = 'testUser'", null);
            }
            else
            {
                totalRows = weatherRepo.Count(request.SearchField + " = " + $"'{request.SearchValue}'", null);
            }
            var test = request.SearchFilters.Where(x => !string.IsNullOrWhiteSpace(x.Value));
            if (test.Count() > 0)
            {
                totalRows = weatherRepo.Count("Username = 'testUser'", null);
            }
            else
            {
                totalRows = weatherRepo.Count(request.SearchField + " = " + $"'{request.SearchValue}'", null);
            }
            decimal totalPagesDec = ((decimal)totalRows/(decimal) request.PageSize);
            int totalPages = (int)Math.Ceiling(totalPagesDec);
            PagedResultViewModel<WeatherForecast> result = new PagedResultViewModel<WeatherForecast>()
            {
                CurrentPage = request.CurrentPage,
                OrderAsc = request.OrderAsc,
                OrderBy = request.OrderBy,
                PageCount = totalPages,
                PageSize = request.PageSize,
                RowCount = totalRows,
                Results = colWeatherForcasts.ToList(),
                SearchField = request.SearchField,
                SearchValue = request.SearchValue
            };
            request.PageCount = totalPages;
            request.RowCount = totalRows;
            result.SearchFilters = request.SearchFilters;
            return result;
        }
        public WeatherForecast CreateForecastAsync(WeatherForecast WeatherForecast)
        {
            var reuslt = weatherRepo.InsertAndReturn<WeatherForecast>(WeatherForecast);

            return reuslt;
        }
        public bool UpdateForecastAsync(WeatherForecast weatherForecast)
        {
            var result = weatherRepo.Update(weatherForecast).ID;
            if (result > 0)
                return true;
            return false;
        }
        public bool DeleteForecastAsync(WeatherForecast weatherForecast)
        {
            weatherForecast.IsDeleted = true;
            var result = weatherRepo.Update(weatherForecast).ID;
            if (result > 0)
                return true;
            return false;
        }

        public Task<WeatherForecast[]> GetForecastAsync(DateTime startDate)
        {
            var rng = new Random();
            return Task.FromResult(Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = startDate.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }).ToArray());
        }
    }
}
