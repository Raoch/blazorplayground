﻿using System.Collections.Generic;

namespace _7iSde.IDataAccess
{
    public interface IRepository<T> where T : class
    {
        T Get(string searchField, object value);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAllWhere(string searchField, object value);
        IEnumerable<T> GetAllPaginatedWhere(int numberOfRows, int pageNumber, string orderBy, string searchField, string searchVal, bool orderAsc);
        IEnumerable<T> ExecuteSql(string sql);
        int Count(string whereClause, object parameters);
        int Insert<T1>(T entity);
        T Update(T entity);
        T InsertAndReturn<T1>(T entity);
        //IEnumerable<T> GetPaginatedList();
    }
}
