﻿using _7Side.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace _7Side.IServices
{
    public interface IPaginationService<T>
    {
        PaginatedResultViewModel<T> GetAllPaginated(PaginatedRequestViewModel request);
        PaginatedResultViewModel<T> GetAllPaginatedWithSearch(PaginatedRequestViewModel request);
    }
}
