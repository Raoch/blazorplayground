﻿
using _7Side.Models;
using System.Collections.Generic;

namespace _7Side.IServices
{
    public interface IPlayerService
    {
        IEnumerable<Player> GetAll();
        Player GetByID(int id);
        int Insert(Player game);
        IEnumerable<Player> GetPlayersByGameID(int gameID);
    }
}
