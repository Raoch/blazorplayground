﻿
using _7Side.Models;
using System.Collections.Generic;

namespace _7Side.IServices
{
    public interface IUserService
    {
        IEnumerable<User> GetAll();
        User GetByID(int id);
        int Insert(User game);
    }
}
