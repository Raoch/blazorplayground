﻿
using _7Side.Models;
using System.Collections.Generic;

namespace _7Side.IServices
{
    public interface IGameService
    {
        IEnumerable<Game> GetAll();
        Game GetByID(int id);
        int Insert(Game game);
        Game Edit(Game game);
    }
}
