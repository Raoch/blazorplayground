﻿using _7Side.Models;
using System.Collections.Generic;

namespace _7Side.IServices
{
    public interface IWeatherForecastService
    {
        IEnumerable<WeatherForecast> GetForecastAsync(string currentUser);
        WeatherForecast CreateForecastAsync(WeatherForecast WeatherForecast);
        PagedResultViewModel<WeatherForecast> GetForecastsPaged(PagedResultBase request);
        bool UpdateForecastAsync(WeatherForecast weatherForecast);
        bool DeleteForecastAsync(WeatherForecast weatherForecast);
    }
}
